import { Injectable } from '@angular/core';
import { Item } from './item';

@Injectable({
  providedIn: 'root'
})
export class ItemsControllerService {

  private items: Item[] = [];

  constructor() { }

  saved(nItem: Item) {
    this.items.push(nItem);
    return this.items;
  }

  getAll() {
    return this.items;
  }

  getLength() {
    return this.items.length;
  }


}
