import { Component, OnInit, ɵConsole } from '@angular/core';
import { Item } from '../item';
import { NumeroToLetrasService } from '../numero-to-letras.service';
import { ItemsControllerService } from '../items-controller.service';
import { FileControllerService } from '../file-controller.service';
import { HistoryServiceService } from '../history-service.service';
import { Item_History } from '../Item_History';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  itemsGenerales: GeneralItem[] = [];
  tmpItems: Item[] = [];
  contador:number = 0;

  metros = "";
  items: Item[] = [];

  html = "";
  item_id: number = 0;
  count_list = 1;

  item_row: Item_History = {
    id: 0,
    date: new Date().toDateString(),
    items: "{}"
  };

  // Models for reset
  est = "";
  pos = "";
  grados = "";
  minutos = "";
  segundos = "";
  valor = "";
  area = "";
  _colindancia = "";

  item : Item = {
    title: "",
    est: "cero",
    est_num: "0",
    pos: "cero",
    pos_num: "0",
    long: "cero",
    long_num: "0",
    grados: "cero",
    grados_num: "0",
    minutos: "cero",
    minutos_num: "0",
    segundos: "cero",
    segundos_num: "0",
    metros: 0,
    colindancia: "",
    constituye: "true",
    divisa: "Q",
    valor: 0,
    valor_str: "cero",
    area: 0,
    area_str: "cero"
  }

  constituye = "true";
  constituye_true: string = "(QUE CONSTITUYE FINCA FILIAL).";
  constituye_false: string = "(QUE NO CONSTITUYE FINCA FILIAL).";
  constituye_str = this.constituye_true;

  classConstituye: string = "";
  classCols: string = "col-4";
  divisas = {
    txt: "QUETZALES",
    sym: "Q. "
  }

  tmpItemGeneral: GeneralItem = {
    header: "",
    items: this.items,
    id: this.contador
  }

  constructor(
    private utils: NumeroToLetrasService,
    private itemsController: ItemsControllerService,
    private fileController: FileControllerService,
    private history: HistoryServiceService,
    private http: HttpClient
    ) { }

  ngOnInit() {

    this.build_html();
    console.log("%c##############################################","color:red");
    console.log("%cApplication developed by SingleCode","color: red");
    console.log("%chttps://www.singlecode.org","color: red");
    console.log("%c##############################################","color: red");
  }

  validate_constituye() {
    if(this.item.constituye=="true") {
      this.constituye_str = this.constituye_true;
      this.classConstituye = "";
      this.classCols = "col-4";
    } else {
      this.constituye_str = this.constituye_false;
      this.classConstituye = "hidden_price";
      this.classCols = "col-12";
    }
    this.build_html();
  //  console.log("Value: " + this.constituye_str);
  }

  validate_divisa() {
    if(this.item.divisa == "Q"){
      this.divisas.txt = "QUETZALES";
      this.divisas.sym = "Q.";
    } else {
      this.divisas.txt = "DÓLARES DE LOS ESTADOS UNIDOS DE AMÉRICA";
      this.divisas.sym = "US$ ";
    }
    let valorInput : HTMLElement = document.querySelector(".input-valor") as HTMLElement;
    valorInput.dispatchEvent(new Event('keyup'));
    this.build_html();
  }

  get_data() {
    this.history.get_all();
  }

  build_complete_html() {
    //console.log(this.itemsGenerales);

    var html = "";
    var tmpRow = "";

    for (let index = 0; index < this.itemsGenerales.length; index++) {
      const element = this.itemsGenerales[index];

      tmpRow = "";

      if(index>0) {
        html += '. ';
      }

      var header = element.header;
      for(var i = 0; i < element.items.length; i++) {
        var el = element.items[i];

        if(i>0) {
          tmpRow += '; ';
        }

        var _metros = Number(el.metros).toLocaleString('en', {maximumSignificantDigits : 21});

        tmpRow  += '<span class="item-result">de la estación '+el.est+' ('+el.est_num+') al punto observado '+el.pos+' ('+el.pos_num+'),'
            + 'con un azimut de: '+el.grados+' grados ('+el.grados_num+'°), '
            + el.minutos+' minutos ('+el.minutos_num+'′), '
            + el.segundos+' segundos ('+el.segundos_num+'″), '
            + 'con una distancia de '+el.long+' metros ('+_metros+' mts)'
            + ' colinda con ' + el.colindancia+"</span>";

      }

      html += header + tmpRow

    }

   // console.log(html);
    this.html = html;
  }

  build_html() {

    var NUMERIC_REGEXP = /[-]{0,1}[\d]*[\.]{0,1}[\d]+/g;
    var title = this.item.title.replace(/-/g," GUION ");
    var colinda = this._colindancia.replace(/-/g," guion ");
    var numbersColinda = colinda.match(NUMERIC_REGEXP);
    var numbers = title.match(NUMERIC_REGEXP);
    if(numbers) {
      for(var n=0; n < numbers.length; n++) {
        title = title.replace(numbers[n], this.utils.numeroALetras(numbers[n],""));
      }
    }

    var grados = this.grados;
    var numbersGrados =  grados.match(NUMERIC_REGEXP);
    if(numbersGrados) {
      for(var n = 0; n < numbersGrados.length; n++) {
        grados = grados.replace(numbersGrados[n], this.utils.numeroALetras(numbersGrados[n], ""));
      }
    }

    this.item.grados = grados;
    if(numbersGrados) {
      this.item.grados_num = numbersGrados[0];
    }

    var _poscicion = this.pos;
    var numbersPos = _poscicion.match(NUMERIC_REGEXP);
    if(numbersPos) {
      for(n=0; n < numbersPos.length; n++) {
        _poscicion = _poscicion.replace(numbersPos[n], this.utils.numeroALetras(numbersPos[n], ""));
      }
    }
    this.item.pos = _poscicion;
    if(numbersPos) {
      this.item.pos_num = numbersPos[0];
    }

    var estacion = this.est;
    var numbersEst = estacion.match(NUMERIC_REGEXP);
    if(numbersEst) {
      for(n=0; n < numbersEst.length; n++) {
        estacion = estacion.replace(numbersEst[n], this.utils.numeroALetras(numbersEst[n], ""));
      }
    }
    this.item.est = estacion;
    if(numbersEst) {
      this.item.est_num = numbersEst[0];
    }

    if(numbersColinda) {
      for(var n=0; n < numbersColinda.length; n++) {
        colinda = colinda.replace(numbersColinda[n], this.utils.numeroALetras(numbersColinda[n],""));
      }
    }

    this.item.colindancia = colinda;

    //console.log();

    console.log(this.item);
    var header = '<strong style="text-transform: uppercase;">';
    header += (title).toUpperCase()+' '+this.constituye_str;
    if(this.item.constituye == "true") {

      var _valor = Number(this.valor).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
     // _valor = Number(_valor).toLocaleString('en');

      header += ' CON UN VALOR DE: '+(this.item.valor_str).toUpperCase()+' ('+this.divisas.sym+' '+_valor+').';
    }
    var _area = Number(this.area).toFixed(2);
    _area = Number(_area).toLocaleString('en', {maximumSignificantDigits : 21});
  header += ' CON UN AREA DE: '+(this.item.area_str).toUpperCase()+' METROS CUADRADOS ('+_area+' MTS²).</strong> '
                + ' Con estaciones, puntos observados, azimuts, distancias y colindancias siguientes: ';

  this.itemsGenerales[this.contador] = {
    header: header,
    items: this.tmpItems,
    id: this.contador
  };

  this.html = header;

    for (let index = 0; index < this.items.length; index++) {
      const element = this.items[index];

      if(index>0) {
        this.html += ', ';
      }

      this.html  += 'de la estación '+element.est+' al punto observado '+element.pos+','
            + 'con un azimut de: '+element.grados+' grados, '
            + element.minutos+' minutos, '
            + element.segundos+' segundos, '
            + 'con una distancia de '+element.long+' metros ('+element.metros+' mts)'
            + ' colinda con ' + element.colindancia;
    }

    this.build_complete_html();
  }

  nuevoPlano() {
    //this.itemsGenerales.push(this.tmpItemGeneral);

    this.contador++;
    this.reset();
    this.tmpItems = [];
    this.tmpItemGeneral = {
      header: "",
      items: [],
      id: this.contador
    }
    this.item.title = "";
    this.valor = "";
    this.area = "";


    this.build_html();
  //  console.log(this.itemsGenerales);
  }

  getValorStr() {
    this.item.valor_str = this.utils.numeroALetras(this.validarNumero(this.valor,null),"",this.divisas.txt);
  }

  numeroALetras(number: any,type: string) {

    this.validate_divisa();

    var _n = Number(number.target.value).toFixed(2);
    _n = Number(_n).toLocaleString('en', {maximumSignificantDigits : 21});

    switch(type) {
      case "est": this.item.est = this.utils.numeroALetras(this.validarNumero(number.target.value, number.target),""); this.item.est_num = _n; break;
      case "pos": this.item.pos = this.utils.numeroALetras(this.validarNumero(number.target.value,number.target),""); this.item.pos_num = _n; break;
      case "long": this.item.long = this.utils.numeroALetras(this.validarNumero(number.target.value,number.target),""); this.item.long_num = _n; break;
      case "grados": this.item.grados = this.utils.numeroALetras(this.validarNumero(number.target.value,number.target),""); this.item.grados_num = _n; break;
      case "minutos": this.item.minutos = this.utils.numeroALetras(this.validarNumero(number.target.value,number.target),""); this.item.minutos_num = _n; break;
      case "segundos": this.item.segundos = this.utils.numeroALetras(this.validarNumero(number.target.value,number.target),""); this.item.segundos_num = _n; break;
      case "valor":
        this.item.valor_str = this.utils.numeroALetras(this.validarNumero(number.target.value,number.target),"",this.divisas.txt);
      //  this.valor = Number(this.valor) .toFixed(2);
        break;
      case "area": this.item.area_str = this.utils.numeroALetras(this.validarNumero(number.target.value,number.target),""); break;
    }
    this.build_html();
  }

  validarNumero(number: any, target: any)  {
    var fn = 0;
    if(number >= 0) {
      fn = number;
    } else {
      fn = 0;
      target.value = "";
    }
    return fn;
  }

  guardar() {
    this.tmpItems.push(this.item);
    this.itemsController.saved(this.item);
    this.items = this.itemsController.getAll();
    this.item_row.items = JSON.stringify(this.items);
    this.build_html();
    this.reset();
    //console.log(this.item_row);
    //this.send_to_server();
  }

  send_to_server() {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': '*',
      })
    };

    if(this.item_row.id == 0) {
      this.http.post("http://app.singlecode.org/api/index.php", {
        date: this.item_row.date,
        items: this.item_row.items,
        update: false
      }, httpOptions).subscribe(response => {
        console.log(response);
        this.item_row.id = parseInt(response.toString());
        this.get_data();
      }, error => {
        console.log(error);
      });
    } else {
      this.http.post("http://app.singlecode.org/api/index.php", {
        date: this.item_row.date,
        items: this.item_row.items,
        update: true,
        id: this.item_row.id
      }, httpOptions).subscribe(response => {
        console.log(response);
        this.item_row.id = parseInt(response.toString());
        this.get_data();
      }, error => {
        console.log(error);
      });
    }
  }

  reset() {

    this.item = {
      title: this.item.title,
      est: "cero",
      est_num: "0",
      pos: "cero",
      pos_num: "0",
      long: "cero",
      long_num: "0",
      grados: "cero",
      grados_num: "0",
      minutos: "cero",
      minutos_num: "0",
      segundos: "cero",
      segundos_num: "0",
      metros: 0,
      colindancia: "",
      constituye: this.item.constituye,
      divisa: this.item.divisa,
      valor: this.item.valor,
      valor_str: this.item.valor_str,
      area: this.item.area,
      area_str: this.item.area_str
    }
    this.est = "";
    this.pos = "";
    this.metros = "";
    this.grados = "";
    this.minutos = "";
    this.segundos = "";

  }

  convert() {
    //console.log(this.html);
    this.fileController.downloadWord(this.html);
  }

  getClass() {
    var id = this.count_list;
    if(this.items.length == 1) {
      return "";
    } else {
      if(this.items.length == id) {
        return "last-result";
      } else {
        if(id == 1) {
          return "first-result";
        } else {
          return "";
        }
      }
    }
    this.count_list = id + 1;
  }

}

export class GeneralItem {
  header: string;
  items: Item[];
  id: number;
}

export class ItemConstruction {
  header: string;
  items: Item[];
  id: number;
}