import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Item_History } from './Item_History';
import { HistoryServiceService } from './history-service.service';
import {MatDialog} from '@angular/material';
import { Item } from './item';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'My First App';

  icon_menu = 'arrow_back';  
  isMenu = true;



  constructor() {}

  ngOnInit() {

  }

}
