import { Injectable } from '@angular/core';
import * as FileSaver from "file-saver";

@Injectable({
  providedIn: 'root'
})
export class FileControllerService {  

  constructor() { }

  downloadWord(html) {
    
    var _static = {
          mhtml: {
              top: "Mime-Version: 1.0\nContent-Base: " + location.href + "\nContent-Type: Multipart/related; boundary=\"NEXT.ITEM-BOUNDARY\";type=\"text/html\"\n\n--NEXT.ITEM-BOUNDARY\nContent-Type: text/html; charset=\"utf-8\"\nContent-Location: " + location.href + "\n\n<!DOCTYPE html>\n<html>\n_html_</html>",
              head: "<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n<style>\n_styles_\n</style>\n</head>\n",
              body: "<body>_body_</body>"
          }
    };

    var markup = '\n<div class="panel-body" style="text-align: justify">\n'+html+'\n</div>\n';
    var mhtmlBottom = '\n--NEXT.ITEM-BOUNDARY--';

    var fileContent = _static.mhtml.top.replace("_html_", _static.mhtml.head.replace("_styles_", "") + _static.mhtml.body.replace("_body_", markup)) + mhtmlBottom;
    console.log(fileContent);
    var data = new Blob([fileContent], { type: 'application/msword;charset=utf-8' });
    FileSaver.saveAs(data, 'LexinCorp'+new Date()+'.doc');
    
  }

}
