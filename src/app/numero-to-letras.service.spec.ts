import { TestBed } from '@angular/core/testing';

import { NumeroToLetrasService } from './numero-to-letras.service';

describe('NumeroToLetrasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NumeroToLetrasService = TestBed.get(NumeroToLetrasService);
    expect(service).toBeTruthy();
  });
});
