import { TestBed } from '@angular/core/testing';

import { ItemsControllerService } from './items-controller.service';

describe('ItemsControllerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ItemsControllerService = TestBed.get(ItemsControllerService);
    expect(service).toBeTruthy();
  });
});
