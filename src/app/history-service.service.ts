import { Injectable, Output, EventEmitter } from '@angular/core';
import { Item_History } from './Item_History';

@Injectable({
  providedIn: 'root'
})
export class HistoryServiceService {

  constructor() { }
  
  @Output() open: EventEmitter<Item_History> = new EventEmitter();
  @Output() get: EventEmitter<Item_History> = new EventEmitter();

  open_history(item:Item_History) {
    this.open.emit(item);    
  }

  get_all() {
    this.get.emit();
  }

}
