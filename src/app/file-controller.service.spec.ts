import { TestBed } from '@angular/core/testing';

import { FileControllerService } from './file-controller.service';

describe('FileControllerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FileControllerService = TestBed.get(FileControllerService);
    expect(service).toBeTruthy();
  });
});
