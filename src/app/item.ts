export class Item {
    title: string;
    est: string;
    est_num: string;
    pos: string;
    pos_num: string;
    long: string;
    long_num: string;
    grados: string;
    grados_num: string;
    minutos: string;
    minutos_num: string;
    segundos: string;
    segundos_num: string;
    metros: number;    
    colindancia: string;
    constituye: string;
    divisa: string;
    valor: number;
    valor_str: string;
    area: number;
    area_str: string;
}